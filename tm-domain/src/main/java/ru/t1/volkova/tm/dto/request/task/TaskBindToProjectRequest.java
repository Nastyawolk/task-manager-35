package ru.t1.volkova.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.dto.request.AbstractUserRequest;
import ru.t1.volkova.tm.enumerated.Status;

@Getter
@Setter
public final class TaskBindToProjectRequest extends AbstractUserRequest {

    @Nullable
    private String projectId;

    @Nullable
    private String taskId;

    public TaskBindToProjectRequest(@Nullable final String token) {
        super(token);
    }

}
