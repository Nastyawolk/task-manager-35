package ru.t1.volkova.tm.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.volkova.tm.api.service.IProjectService;
import ru.t1.volkova.tm.enumerated.Status;
import ru.t1.volkova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.volkova.tm.exception.field.DescriptionEmptyException;
import ru.t1.volkova.tm.exception.field.IdEmptyException;
import ru.t1.volkova.tm.exception.field.IndexIncorrectException;
import ru.t1.volkova.tm.exception.field.NameEmptyException;
import ru.t1.volkova.tm.model.Project;
import ru.t1.volkova.tm.repository.ProjectRepository;
import ru.t1.volkova.tm.service.ProjectService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ProjectServiceTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    private static final String USER_ID_1 = UUID.randomUUID().toString();

    private static final String USER_ID_2 = UUID.randomUUID().toString();

    @NotNull
    private List<Project> projectList;

    @NotNull
    private IProjectService projectService;

    @Before
    public void initRepository() {
        projectList = new ArrayList<>();
        projectService = new ProjectService(new ProjectRepository());
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final Project project = new Project();
            project.setName("project" + i);
            project.setDescription("description" + i);
            if (i <= 5) project.setUserId(USER_ID_1);
            else project.setUserId(USER_ID_2);
            projectService.add(project);
            projectList.add(project);
        }
    }

    @Test
    public void testCreate(
    ) {
        @Nullable final Project project = projectService.create(USER_ID_1, "new_project", "new description");
        if (project == null) return;
        Assert.assertEquals(project, projectService.findOneById(project.getId()));
        Assert.assertEquals(NUMBER_OF_ENTRIES + 1, projectService.getSize().intValue());
    }

    @Test
    public void testCreateNegative(
    ) {
        @Nullable final Project project = projectService.create(null, "new_project", "new description");
        Assert.assertNull(project);
    }

    @Test(expected = NameEmptyException.class)
    public void testCreateNameEmpty(
    ) {
        projectService.create(USER_ID_1, null, "new description");
    }

    @Test(expected = DescriptionEmptyException.class)
    public void testCreateDescriptionEmpty(
    ) {
        projectService.create(USER_ID_2, "new", null);
    }

    @Test
    public void testUpdateById() {
        @NotNull final String newName = "new project";
        @NotNull final String newDescription = "new project";
        @NotNull final String id = projectList.get(0).getId();
        projectService.updateById(USER_ID_1, id, newName, newDescription);
        @Nullable final Project project = projectService.findOneById(USER_ID_1, id);
        if (project == null) return;
        Assert.assertEquals(newName, project.getName());
        Assert.assertEquals(newDescription, project.getDescription());
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testUpdateNotFoundProject(
    ) {
        @NotNull final String newName = "new project";
        @NotNull final String newDescription = "new project";
        @NotNull final String id = "non-existent-id";
        projectService.updateById(USER_ID_1, id, newName, newDescription);
    }

    @Test(expected = IdEmptyException.class)
    public void testUpdateIdEmpty(
    ) {
        projectService.updateById(USER_ID_1, null, "name", "new description");
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateNameEmpty(
    ) {
        projectService.updateById(USER_ID_1, projectList.get(0).getId(), null, "new description");
    }

    @Test(expected = DescriptionEmptyException.class)
    public void testUpdateDescriptionEmpty(
    ) {
        projectService.updateById(USER_ID_2, projectList.get(0).getId(), "name", null);
    }

    @Test
    public void testUpdateByIndex() {
        @NotNull final String newName = "new project";
        @NotNull final String newDescription = "new project";
        projectService.updateByIndex(USER_ID_1, 2, newName, newDescription);
        @Nullable final Project project = projectService.findOneByIndex(USER_ID_1, 2);
        if (project == null) return;
        Assert.assertEquals(newName, project.getName());
        Assert.assertEquals(newDescription, project.getDescription());
    }

    @Test(expected = IndexIncorrectException.class)
    public void testUpdateIdEmptyByIndex(
    ) {
        projectService.updateByIndex(USER_ID_1, 10, "name", "new description");
        projectService.updateByIndex(USER_ID_1, null, "name", "new description");
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateNameEmptyByIndex(
    ) {
        projectService.updateByIndex(USER_ID_1, 2, null, "new description");
    }

    @Test(expected = DescriptionEmptyException.class)
    public void testUpdateDescriptionEmptyByIndex(
    ) {
        projectService.updateByIndex(USER_ID_2, 3, "name", null);
    }

    @Test
    public void testChangeStatusById() {
        @NotNull final String id = projectList.get(0).getId();
        @Nullable final Project project = projectService.changeProjectStatusById(USER_ID_1, id, Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());
    }

    @Test(expected = IdEmptyException.class)
    public void testChangeStatusIdEmptyById() {
        projectService.changeProjectStatusById(USER_ID_1, null, Status.IN_PROGRESS);
        projectService.changeProjectStatusById(USER_ID_1, "", Status.IN_PROGRESS);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testChangeStatusProjectNotFoundById() {
        projectService.changeProjectStatusById("USER_ID_12", "id", Status.IN_PROGRESS);
        projectService.changeProjectStatusById(USER_ID_1, "non-existent", Status.IN_PROGRESS);
    }

    @Test
    public void testChangeStatusByIndex() {
        @Nullable final Project project = projectService.changeProjectStatusByIndex(USER_ID_1, 2, Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());
    }

    @Test(expected = IndexIncorrectException.class)
    public void testChangeStatusIndexExceptionByIndex() {
        projectService.changeProjectStatusByIndex(USER_ID_1, 22, Status.IN_PROGRESS);
        projectService.changeProjectStatusByIndex(USER_ID_1, null, Status.IN_PROGRESS);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testChangeStatusProjectNotFoundByIndex() {
        projectService.changeProjectStatusById("USER_ID_12", "id", Status.IN_PROGRESS);
        projectService.changeProjectStatusById(USER_ID_1, "non-existent", Status.IN_PROGRESS);
    }

    @Test
    public void testFindOneById() {
        Assert.assertNotNull(projectService.findOneById(USER_ID_1, projectList.get(0).getId()));
        Assert.assertNotNull(projectService.findOneById(USER_ID_2, projectList.get(7).getId()));
    }

    @Test(expected = ProjectNotFoundException.class)
    public void testFindOneByIdNegative() {
        Assert.assertNotNull(projectService.findOneById(USER_ID_1, "non-existent"));
    }

}
