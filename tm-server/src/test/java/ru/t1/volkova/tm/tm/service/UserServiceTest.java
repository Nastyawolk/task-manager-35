package ru.t1.volkova.tm.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.volkova.tm.api.service.IUserService;
import ru.t1.volkova.tm.enumerated.Role;
import ru.t1.volkova.tm.exception.entity.UserNotFoundException;
import ru.t1.volkova.tm.exception.field.EmailEmptyException;
import ru.t1.volkova.tm.exception.field.IdEmptyException;
import ru.t1.volkova.tm.exception.field.LoginEmptyException;
import ru.t1.volkova.tm.exception.field.PasswordEmptyException;
import ru.t1.volkova.tm.exception.user.LoginExistsException;
import ru.t1.volkova.tm.exception.user.RoleEmptyException;
import ru.t1.volkova.tm.model.User;
import ru.t1.volkova.tm.repository.ProjectRepository;
import ru.t1.volkova.tm.repository.TaskRepository;
import ru.t1.volkova.tm.repository.UserRepository;
import ru.t1.volkova.tm.service.PropertyService;
import ru.t1.volkova.tm.service.UserService;

import java.util.ArrayList;
import java.util.List;


public class UserServiceTest {

    private static final int NUMBER_OF_ENTRIES = 4;

    @NotNull
    private List<User> userList;

    @NotNull
    private IUserService userService;

    @Before
    public void initRepository() {
        userList = new ArrayList<>();
        userService = new UserService
                (
                        new UserRepository(),
                        new ProjectRepository(),
                        new TaskRepository(),
                        new PropertyService()
                );

        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final User user = new User();
            user.setLogin("user" + i);
            user.setEmail("user@" + i + ".ru");
            user.setRole(Role.USUAL);
            userService.add(user);
            userList.add(user);
        }
    }

    @Test
    public void testCreate(
    ) {
        @Nullable final User user = userService.create("new_login", "password332");
        Assert.assertEquals(user, userService.findOneById(user.getId()));
        Assert.assertEquals(NUMBER_OF_ENTRIES + 1, userService.getSize().intValue());
    }

    @Test
    public void testCreateEmail(
    ) {
        @Nullable final User user = userService.create("new_login", "password332", "new_user@mail.ru");
        Assert.assertEquals(user, userService.findOneById(user.getId()));
        Assert.assertEquals(NUMBER_OF_ENTRIES + 1, userService.getSize().intValue());
    }

    @Test
    public void testCreateRole(
    ) {
        @Nullable final User user = userService.create("admin2", "password332", Role.ADMIN);
        Assert.assertEquals(user, userService.findOneById(user.getId()));
        Assert.assertEquals(NUMBER_OF_ENTRIES + 1, userService.getSize().intValue());
        Assert.assertEquals(Role.ADMIN, user.getRole());
    }

    @Test(expected = LoginEmptyException.class)
    public void testCreateLoginEmpty(
    ) {
        userService.create(null, "password332");
        userService.create("", "password332");
    }

    @Test(expected = LoginExistsException.class)
    public void testCreateLoginExists(
    ) {
        userService.create("user2", "password332");
    }

    @Test(expected = PasswordEmptyException.class)
    public void testCreatePasswordEmpty(
    ) {
        userService.create("new_user", null);
        userService.create("new_user", "");
    }

    @Test(expected = EmailEmptyException.class)
    public void testCreateEmailEmpty(
    ) {
        userService.create("new_login", "password332", "");
        userService.create("new_login", "password332", (String) null);
    }

    @Test(expected = RoleEmptyException.class)
    public void testCreateRoleEmpty(
    ) {
        userService.create("new_login", "password332", (Role) null);
    }

    @Test
    public void testFindByLogin() {
        Assert.assertEquals(userList.get(1), userService.findByLogin("user2"));
        Assert.assertEquals(userList.get(0), userService.findByLogin("user1"));
    }

    @Test
    public void testFindByLoginNegative() {
        Assert.assertNull(userService.findByLogin("non-existent"));
    }

    @Test(expected = LoginEmptyException.class)
    public void testFindByLoginEmpty() {
        Assert.assertEquals(userList.get(1), userService.findByLogin(""));
        Assert.assertEquals(userList.get(0), userService.findByLogin(null));
    }

    @Test
    public void testFindByEmail() {
        Assert.assertEquals(userList.get(1), userService.findByEmail("user@2.ru"));
        Assert.assertEquals(userList.get(0), userService.findByEmail("user@1.ru"));
    }

    @Test
    public void testFindByEmailNegative() {
        Assert.assertNull(userService.findByEmail("non-existent"));
    }

    @Test(expected = EmailEmptyException.class)
    public void testFindByEmailEmpty() {
        Assert.assertEquals(userList.get(1), userService.findByEmail(""));
        Assert.assertEquals(userList.get(0), userService.findByEmail(null));
    }

    @Test
    public void testRemoveOne() {
        Assert.assertEquals(userList.get(1), userService.removeOne(userList.get(1)));
        Assert.assertEquals(NUMBER_OF_ENTRIES - 1, userService.getSize().intValue());
    }

    @Test
    public void testRemoveOneNegative() {
        Assert.assertNull(userService.removeOne(null));
    }

    @Test(expected = UserNotFoundException.class)
    public void testRemoveOneUserNotFound() {
        userService.removeOne(new User());
    }

    @Test
    public void testRemoveByLogin() {
        Assert.assertEquals(userList.get(1), userService.removeByLogin(userList.get(1).getLogin()));
        Assert.assertEquals(NUMBER_OF_ENTRIES - 1, userService.getSize().intValue());
    }

    @Test(expected = LoginEmptyException.class)
    public void testRemoveByLoginEmptyLogin() {
        userService.removeByLogin(null);
        userService.removeByLogin("");
    }

    @Test
    public void testRemoveByEmail() {
        Assert.assertEquals(userList.get(1), userService.removeByEmail(userList.get(1).getEmail()));
        Assert.assertEquals(NUMBER_OF_ENTRIES - 1, userService.getSize().intValue());
    }

    @Test(expected = EmailEmptyException.class)
    public void testRemoveByLoginEmptyEmail() {
        userService.removeByEmail(null);
        userService.removeByEmail("");
    }

    @Test
    public void testSetPassword() {
        Assert.assertNotNull(userService.setPassword(userList.get(0).getId(), "newPass"));
    }

    @Test(expected = IdEmptyException.class)
    public void testSetPasswordEmptyId() {
        userService.setPassword(null, "newPass");
        userService.setPassword("", "newPass");
    }

    @Test(expected = PasswordEmptyException.class)
    public void testSetPasswordEmptyPassword() {
        userService.setPassword(userList.get(1).getId(), "");
        userService.setPassword(userList.get(1).getId(), null);
    }

    @Test
    public void testUpdateUser() {
        @NotNull final String firstName = "new name";
        @NotNull final String lastName = "new lastName";
        @NotNull final String middleName = "new middleName";
        @NotNull final String id = userList.get(0).getId();
        userService.updateUser(id, firstName, lastName, middleName);
        @Nullable final User user = userService.findOneById(id);
        if (user == null) return;
        Assert.assertEquals(firstName, user.getFirstName());
        Assert.assertEquals(lastName, user.getLastName());
        Assert.assertEquals(middleName, user.getMiddleName());
    }

    @Test(expected = UserNotFoundException.class)
    public void testUpdateNotFoundTask(
    ) {
        @NotNull final String firstName = "new name";
        @NotNull final String lastName = "new lastName";
        @NotNull final String middleName = "new middleName";
        userService.updateUser("non-existent", firstName, lastName, middleName);
    }

    @Test(expected = IdEmptyException.class)
    public void testUpdateIdEmpty(
    ) {
        userService.updateUser("", "firstName", "lastName", "middleName");
        userService.updateUser(null, "firstName", "lastName", "middleName");
    }

    @Test
    public void testIsLoginExist() {
        Assert.assertTrue(userService.isLoginExist("user4"));
    }

    @Test
    public void testIsLoginExistNegative() {
        Assert.assertFalse(userService.isLoginExist("user4444"));
        Assert.assertFalse(userService.isLoginExist(""));
        Assert.assertFalse(userService.isLoginExist(null));
    }

    @Test
    public void testIsEmailExist() {
        Assert.assertTrue(userService.isEmailExist("user@4.ru"));
    }

    @Test
    public void testLockUserByLogin() {
        @NotNull final User user = userService.lockUserByLogin(userList.get(0).getLogin());
        Assert.assertTrue(user.getLocked());
    }

    @Test(expected = LoginEmptyException.class)
    public void testLockUserByEmptyLogin() {
        userService.lockUserByLogin("");
        userService.lockUserByLogin(null);
    }

    @Test(expected = UserNotFoundException.class)
    public void testLockUserByEmptyId() {
        userService.lockUserByLogin("non-existent");
    }

    @Test
    public void testUnlockUserByLogin() {
        @NotNull final User user = userService.unlockUserByLogin(userList.get(0).getLogin());
        Assert.assertFalse(user.getLocked());
    }

    @Test(expected = LoginEmptyException.class)
    public void testUnlockUserByEmptyLogin() {
        userService.unlockUserByLogin("");
        userService.unlockUserByLogin(null);
    }

    @Test(expected = UserNotFoundException.class)
    public void testUnlockUserByEmptyId() {
        userService.unlockUserByLogin("non-existent");
    }

    @Test
    public void testFindOneById() {
        Assert.assertNotNull(userService.findOneById(userList.get(0).getId()));
        Assert.assertNotNull(userService.findOneById(userList.get(2).getId()));
    }

    @Test
    public void testFindOneByIdNegative() {
        Assert.assertNull(userService.findOneById("non-existent"));
    }

}
