package ru.t1.volkova.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.dto.request.data.DataXmlLoadJaxBRequest;
import ru.t1.volkova.tm.enumerated.Role;

public final class DataXmlLoadJaxBCommand extends AbstractDataCommand {

    @NotNull
    public static final String DESCRIPTION = "Load data from XML file";

    @NotNull
    public static final String NAME = "data-load-xml-jaxb";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA LOAD XML]");
        serviceLocator.getDomainEndpoint().loadDataXmlJaxB(new DataXmlLoadJaxBRequest(getToken()));
    }

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

}
